module.exports = function(grunt){
    
    grunt.initConfig({
        jshint: {
            all: ['gruntfile.js', 'app.js','models/**/*.js','routes/**/*.js','services/**/*.js'],
            options: {
                globals: {
                    jQuery: true,
                    console: true,
                    module: true,
                    document: true
                }
            }
        },
        watch: {
            hints:{
                files:  ['gruntfile.js', 'app.js','models/**/*.js','routes/**/*.js','services/**/*.js'],
                tasks: ['jshint'],
                options: {
                    spawn: true
                },
            }
        }//watch

    });

    //grunt.loadNpmTasks('grunt-contrib-copy');
    //grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    
    grunt.registerTask('default',['jshint','wiredep','watch']);
    
};