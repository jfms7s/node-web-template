module.exports=function(req,res,next){
    
    var Exeption = /^(\/[Uu]sers)?((\/([Cc]reate|[Ll]ogin))|\/)?$/;
    var isAuthenticated = req.isAuthenticated();
    console.log(req.originalUrl,/^(\/public\/)/.test(req.originalUrl));
    if(/^(\/public\/)/.test(req.originalUrl)){
        return next();
    }

    if(Exeption.test(req.originalUrl))
        if(!isAuthenticated)
            return next();
        else
            return res.redirect("/UserProfile");
    
    if(isAuthenticated)
        return next();
    
    res.redirect("/");
}