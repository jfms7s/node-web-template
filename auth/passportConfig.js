module.exports = function(){
    var passport = require("passport");
    var passportLocal = require("passport-local");
    var userService = require("../services/userService");
    var bcrypt = require("bcrypt");
    
    passport.use(new passportLocal.Strategy({usernameField:"email"},function(email,password,next){
        userService.findOne({email:email},function(err,user){
            if(err){
                return next(err);
            }
            if(!user){
                return next(null,null);
            }
            
            bcrypt.compare(password,user.password,function(err,same){
                if(err){
                    return next(err);
                }
                if(same){
                    return next(null,user);
                }
                next(null,null);
                
            });
        });
    }));
    
    passport.serializeUser(function(user,next) {
        next(null,user.email);
    });
    
    passport.deserializeUser(function(email,next){
        userService.findOne({email:email},function(err,user){
            next(err,user);
        });
    });
};

/*
passport.use(new TwitterStrategy(config.twitter, function(req,accessToken, tokenSecret, profile, next) {
    User.findOne({ twitter: profile.id }, function(err,user) {
        if (user)
            return done(null, existingUser);
        var user = {}

        user.twitter = profile.id;
        user.tokens.push({ kind: 'twitter', accessToken:accessToken, tokenSecret: tokenSecret });
        //fill rest of the data depending on API

        userService.add(user,next);
    });
}));
*/