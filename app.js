var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

//Notifications
var flash = require("connect-flash");

//mongodb driver
var mongoose = require('mongoose');

//session stuff
var connectMongo = require("connect-mongo");
var expressSession = require("express-session");

//auth stuff
var passport = require("passport");
var passportConfig = require("./auth/passportConfig");
var restrict = require("./auth/restrict");
passportConfig();

//mongo connection and sessionStorage in mongo
var config = require(process.env.UNIT_TEST || './config.json');
mongoose.connect(config.database.uri);
var MongoStore = connectMongo(expressSession);

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
//-->>
//normal express session
app.use(expressSession({
    secret:  '1234567890QWERTY',
    resave: false,
    saveUninitialized:false,
    store:new MongoStore({
        mongooseConnection: mongoose.connection
    })
}));

app.use(flash());//get notifications in session
app.use(passport.initialize());//passport session related stuff
app.use(passport.session());

app.use(restrict);//routing restrictions
require('./routes/')(app);//route defenition

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


module.exports = app;
