var User = require("../models/user");
var BaseService = require("../baseClasses/baseService");
var bcrypt = require("bcrypt");

var userService = new BaseService(User);

userService.addUser = function(model,next){
    var self = this;
    self.create(model,next);
};

module.exports = userService;