module.exports = function(app) {

  var fs        = require("fs");
  var path      = require("path");

  var StaticRoutePrefix = "";//route prefix
  var prefixException = ['users'];//filename exeptions(file name is not used as route name)

    //TODO check https://github.com/isaacs/node-glob

  fs  .readdirSync(__dirname)
      .filter(function(file) {
        return (file.indexOf(".") !== 0) && (file !== "index.js") && (file !== "api");
      })
      .forEach(function(file) {
        var routeName = file.substr(0, file.lastIndexOf('.'));

        var route=require(path.join(__dirname, file));

        var address= StaticRoutePrefix + (prefixException.indexOf(routeName) > -1?'(/' + routeName+')?': '/' + routeName);
        app.use(address , route);
      });

};