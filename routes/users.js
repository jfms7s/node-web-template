var express = require('express');
var router = express.Router();

var userService = require("../services/userService");
var passport = require("passport");

var config = require("../config");


router.get(/^\/([Ll]ogin)?$/, function(req, res, next) {
	var vm = {
		title:"User Login",
		error:req.flash("error")
	};
	
	res.render("users/login",vm);
});

router.post("/login",function(req, res, next) {
    if(req.body.rememberMe)
    	req.session.cookie.maxAge = config.cookieMaxAge;
    	
    next();
},passport.authenticate('local',{failureRedirect:"/",successRedirect:"/users/profile",failureFlash:"Invalid credentials"}));

router.get('/create', function(req, res, next) {
	var vm = {title:"Create Account",input:{}};
	
	res.render("users/create",vm);
});

router.post('/create', function(req, res, next) {
	var user = req.body;

	userService.addUser(user,function(err){
		if(err){
			var vm = {
				title:"Create Account",
				input:user,
				error:"Something went wrong"
			};
			delete vm.input.password;
			return res.render("users/create",vm);
		}

        res.redirect("/users/login");
	});
});

router.get("/profile",function(req,res,next){
    var vm = {
        title:"Create Account",
        input:req.user,
        error:"Something went wrong"
    };
    res.render("users/profile",vm);
});

router.get("/logout",function(req,res,next){
	req.logout();
	req.session.destroy();
	res.redirect("/");
});


module.exports = router;
