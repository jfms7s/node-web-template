var mongoose = require('mongoose');
var validator = require('validator');
var bcrypt = require('bcrypt');

var userSchema = new mongoose.Schema({
    email       : {type: String, required : true, unique: true},
    password    : {type: String, required : true},
    created_at  : {type: Date  , default  : Date.now},

    facebook    : String,
    twitter     : String,
    google      : String,
    github      : String,
    profile : {
        name: { type: String, default: '' },
        gender: { type: String, default: '' },
        location: { type: String, default: '' },
        website: { type: String, default: '' },
        picture: { type: String, default: '' }
    }
});

userSchema.pre('save', function(next) {
    var self = this;

    if(this.isModified('email')){
        this.email= this.email.toLowerCase();
    }
    if (this.isModified('password')) {
        bcrypt.hash(this.password,10,function(err,hash){
            if(err)
                return next(err);

            self.password=hash;
            next();
        });
    }else
        next();
});

userSchema.path('email').validate(function(email) {
    return validator.isEmail(email);
});

module.exports = mongoose.model('User', userSchema);