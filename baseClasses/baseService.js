module.exports = function(model){
    var self = this;
    
    self.create=function(data,next){
        var newModel = new model(data);
        newModel.save(function(err){
            if(err)
                return next(err,null);
            next(null,newModel);
        });
    };

    self.findOne = function(query,next){
        model.findOne(query,function(err,data){
            next(err,data);
        });
    };

    self.find = function(query,next){
        model.find(query,function(err,data){
            next(err,data);
        });
    };

    self.update = function(query,data,next){
        model.update(query, { $set: data },function(err){
            if(err)
                return next(err,null);
            model.find(query,function(err,data){
                if(err){
                    return next(err,null);
                }
                if(data){
                    next(null,data);
                }
                next(null,null);
            });
        });
    };

    self.remove = function(query,next){
        model.remove(query,function(err){
            if(err)
                return next(err);
            next(null);
        });
    };
    
};