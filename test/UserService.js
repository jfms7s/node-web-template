var chai = require("chai");

var config = require('../unit-test-config.json');

var   expect   = chai.expect
    , mongoose = require('mongoose')
    , clearDB  = require('mocha-mongoose')(config.database.uri, {noClear: true});

var userService = require("../services/userService");
var userModel = require("../models/user.js");
describe('Users Service', function() {

    before(function(done) {
        if (mongoose.connection.db)
            return done();
        mongoose.connect(config.database.uri,done);
    });

    beforeEach("Clear DB",function(done) {
        clearDB(function(){
            userModel.collection.insert([{email: "test@test.test",password:"test"},{email: "test1@test.test",password:"test"},{email: "test2@test.test",password:"test"}],done);
        });
    });

    describe("Create",function(){

        it("one user", function(done) {
            userService.create({email: "testemail@test.test",password:"test"}, function(err,user){
                expect(err).to.be.null;
                expect(user).to.be.not.null;
                expect(user.isNew).to.be.false;
                done();
            });
        });
        it("two Users", function(done) {
            userService.create({email: "testemail1@test.test",password:"test"}, function(err,user){
                expect(err).to.be.null;
                expect(user).to.be.not.null;
                expect(user.isNew).to.be.false;
                done();
            });
        });
    });


    describe("Read",function(){

        it("by email", function(done) {
            userService.findOne({email: "test@test.test"},function(err,user){
                expect(err).to.be.not.undefined;
                expect(user).to.have.property("email","test@test.test");
                done();
            });
        });

        it("all", function(done) {
            userService.find({}, function(err,users){
                expect(err).to.be.null;
                expect(users).to.be.length(3);
                done();
            });
        });
    });

    describe("Update",function(){
        it("by email", function(done) {
            userService.update({email: "test@test.test"},{profile:{name:"test user"}},function(err,users){

                expect(err).to.be.null;
                expect(users).to.be.length(1);
                expect(users[0].isNew).to.be.false;
                expect(users[0].profile).to.have.property("name","test user");
                done();
            });
        });
    });

    describe("Delete",function(){
        it("by email", function(done) {
            userService.remove({email: "test@test.test"},function(err){
                expect(err).to.be.null;

                userService.find({}, function(err,users){
                    expect(err).to.be.null;
                    expect(users).to.be.length(2);
                    done();
                });
            });
        });
        it("all", function(done) {
            userService.remove({}, function(err,users){
                expect(err).to.be.null;

                userService.find({}, function(err,users) {
                    expect(err).to.be.null;
                    expect(users).to.be.length(0);
                    done();

                });
            });
        });
    });
});