var chai = require("chai");

var config = require('../unit-test-config.json');

var   expect   = chai.expect
    , mongoose = require('mongoose')
    , clearDB  = require('mocha-mongoose')(config.database.uri, {noClear: true});

var userService = require("../services/userService");
var userModel = require("../models/user.js");

var request = require('supertest');

var app = require('../app');

describe('Users', function() {

    before(function(done) {
        if (mongoose.connection.db)
            return done();
        mongoose.connect(config.database.uri,done);
    });

    beforeEach("Clear DB",function(done) {
        clearDB(function(){
            userService.create({email: "test@test.test",password:"test"},done);
        });
    });

    describe('registration', function() {
        it('should register valid user', function(done) {
            request(app)
                .post('/users/create')
                .send({
                    email: "test@example.com",
                    password: "testPass"
                })
                .expect(302)
                .end(function(err, res) {
                    userService.find({email: "test@example.com"},function(err,users){
                        expect(users).to.be.length(1);
                        done(err);
                    });

                });
        });
    });
    describe('login', function() {
        it('should login with valid user', function(done) {
            request(app)
                .post('/users/login')
                .send({
                    email: "test@test.test",
                    password: "test"
                })
                .expect(302)
                .end(function(err, res) {
                    expect(res.text).to.be.equal("Found. Redirecting to /users/profile");
                    done();
                });
        });
    });


});